-- grant for existing objects and for furture 
-- objects created by current role (FOR TO is missing, so by default it's the
-- current logged role)
GRANT ALL ON DATABASE d TO db_all;                                   
GRANT ALL ON SCHEMA v TO db_all;                                 
ALTER DEFAULT PRIVILEGES                                                        
    IN SCHEMA v                                                      
    GRANT ALL ON TABLES TO db_all;                                          
ALTER DEFAULT PRIVILEGES                                                        
    IN SCHEMA v                                                      
    GRANT ALL ON FUNCTIONS TO db_all;                                       
ALTER DEFAULT PRIVILEGES                                                        
    IN SCHEMA v                                                      
    GRANT ALL ON SEQUENCES TO db_all;                                       
ALTER DEFAULT PRIVILEGES                                                        
    IN SCHEMA v                                                      
    GRANT ALL ON TYPES TO db_all;                                           
                                                                                
-- grant for future objects created by a specific role 
-- (dba1) to dba_all (dba2 & dba3) and for object created
-- by db_all (dba2 & dba3) to dba1
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE dba1                                                     
    IN SCHEMA v                                                      
    GRANT ALL ON TABLES TO db_all;                                          
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE db_all                                                         
    IN SCHEMA v                                                      
    GRANT ALL ON TABLES TO dba1;                                      
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE dba1                                                     
    IN SCHEMA v                                                      
    GRANT ALL ON FUNCTIONS TO db_all;                                       
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE db_all                                                         
    IN SCHEMA v                                                      
    GRANT ALL ON FUNCTIONS TO dba1;                                   
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE dba1                                                     
    IN SCHEMA v                                                      
    GRANT ALL ON SEQUENCES TO db_all;                                       
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE db_all                                                         
    IN SCHEMA v                                                      
    GRANT ALL ON SEQUENCES TO dba1;                                   
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE dba1                                                     
    IN SCHEMA v                                                      
    GRANT ALL ON TYPES TO db_all;                                           
ALTER DEFAULT PRIVILEGES FOR ROLE                                               
    db_all                                                                  
    IN SCHEMA v                                                      
    GRANT ALL ON TYPES TO dba1;                                       
                                                                                
-- grant for exisitng object to specific meta role (db_all)
GRANT ALL ON ALL TABLES IN SCHEMA v TO db_all;                   
GRANT ALL ON ALL FUNCTIONS IN SCHEMA v TO db_all;                
GRANT ALL ON ALL SEQUENCES IN SCHEMA v TO db_all;
