-- grant for existing objects and for furture object created by current role (FOR TO is missing, so by default it's the
    -- current logged role)
GRANT ALL ON DATABASE d TO dba2,dba3;                                   
GRANT ALL ON SCHEMA v TO dba2,dba3;                                 
ALTER DEFAULT PRIVILEGES                                                        
    IN SCHEMA v                                                      
    GRANT ALL ON TABLES TO dba2,dba3;                                          
ALTER DEFAULT PRIVILEGES                                                        
    IN SCHEMA v                                                      
    GRANT ALL ON FUNCTIONS TO dba2,dba3;                                       
ALTER DEFAULT PRIVILEGES                                                        
    IN SCHEMA v                                                      
    GRANT ALL ON SEQUENCES TO dba2,dba3;                                       
ALTER DEFAULT PRIVILEGES                                                        
    IN SCHEMA v                                                      
    GRANT ALL ON TYPES TO dba2,dba3;                                           
                                                                                
-- grant for future objects created by a specific role (dba1) to dba2 & dba3
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE dba1                                                     
    IN SCHEMA v                                                      
    GRANT ALL ON TABLES TO dba2,dba3;                                          
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE dba2,dba3                                                         
    IN SCHEMA v                                                      
    GRANT ALL ON TABLES TO dba1;                                      
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE dba1                                                     
    IN SCHEMA v                                                      
    GRANT ALL ON FUNCTIONS TO dba2,dba3;                                       
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE dba2,dba3                                                         
    IN SCHEMA v                                                      
    GRANT ALL ON FUNCTIONS TO dba1;                                   
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE dba1                                                     
    IN SCHEMA v                                                      
    GRANT ALL ON SEQUENCES TO dba2,dba3;                                       
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE dba2,dba3                                                         
    IN SCHEMA v                                                      
    GRANT ALL ON SEQUENCES TO dba1;                                   
ALTER DEFAULT PRIVILEGES                                                        
    FOR ROLE dba1                                                     
    IN SCHEMA v                                                      
    GRANT ALL ON TYPES TO dba2,dba3;                                           
ALTER DEFAULT PRIVILEGES FOR ROLE                                               
    dba2,dba3                                                                  
    IN SCHEMA v                                                      
    GRANT ALL ON TYPES TO dba1;                                       
                                                                                
-- grant for exisitng object to specific users (dba2, dba3)
GRANT ALL ON ALL TABLES IN SCHEMA v TO dba2,dba3;                   
GRANT ALL ON ALL FUNCTIONS IN SCHEMA v TO dba2,dba3;                
GRANT ALL ON ALL SEQUENCES IN SCHEMA v TO dba2,dba3;
