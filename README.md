# Defining profile

This repository contains examples / playground for altering default
privileges on specific schema on PostgreSQL database.

In our example we will use:

-   3 roles `dba1`, `dba2`, `dba2`
-   database called `d` owned by `dba1`, that role (`dba1`) can grant
    privileges to
-   a schema `v`

## init

``` console
$ pglift instance create i1
$ pglift instance exec i1 -- psql -f init.sql
$ pglift instance exec i1 -- psql -Udba1 d -c "create schema v;"
$ pglift instance exec i1 -- psql -Udba1 d -c "CREATE TABLE v.weather(v VARCHAR(50));"
```

## Basic examples

With all those objects created and configured, the role `dba1` can:

-   Give some privileges to (by example) `dba2` on **already** existing
    objects with the GRANT command. `dba1` can also use the GRANT
    command to give permission on all objects present (Eg: tables) on
    specific schema. If we want to give `SELECT` permission on table
    `weather` to `dba2`, `dba1` can run that query:

    ``` console
    $ pglift instance exec i1 -- psql -Udba1 d -c "GRANT SELECT ON TABLE v.weather TO dba2"
    ```

    We also need to give USAGE privilege on the specific schema for
    dba2:

    ``` console
    $ pglift instance exec i1 -- psql -Udba1 d -c "GRANT USAGE ON SCHEMA v TO dba2"
    GRANT
    ```

-   Give select permission for all object already on the v schema (still
    for `dba2`), so let's create a new table and try to read its
    content:

    ``` console
    $ pglift instance exec i1 -- psql -Udba1 d -c "CREATE TABLE v.weather2(v VARCHAR(50));"
    $ pglift instance exec i1 -- psql -Udba2 d -c "SELECT * from v.weather2;"              
    ERROR:  permission denied for table weather2
    ```

    `dba2` can't read the content of that table because `dba1` didn't
    `GRANT` the `SELECT` permission, `dba2` can either grant that
    permission for the table or grant for all tables on schema `v`.

        $ pglift instance exec i1 -- psql -Udba1 d -c "GRANT SELECT ON ALL TABLES IN SCHEMA v TO dba2"
        $ pglift instance exec i1 -- psql -Udba2 d -c "SELECT * from v.weather2;"                     
         v 
        ---
        (0 rows)

    `GRANT SELECT ON ALL TABLES` can be tricky because it's only valid
    for already existing objects, if add new table, `dba2` won't be able
    to SELECT content on that table because it was created after the
    `GRANT`:

    ``` console
    $ pglift instance exec i1 -- psql -Udba1 d -c "CREATE TABLE v.weather3(v VARCHAR(50));"
    $ pglift instance exec i1 -- psql -Udba2 d -c "SELECT * from v.weather3;"              
    ERROR:  permission denied for table weather3
    ```

-   `dba1` can also alter the default privileges to determine the
    permission for future objects. By example if we want to give the
    SELECT permission on tables dba1 will create:

    ``` console
    $  pglift instance exec i1 -- psql -Udba1 d -c "ALTER DEFAULT PRIVILEGES IN SCHEMA v GRANT SELECT ON TABLES TO dba2;"
    ```

    Now dba2 can run SELECT on new table(s) created by dba1 without
    re-runing specific GRANT command:

    ``` console
    $ pglift instance exec i1 -- psql -Udba1 d -c "CREATE TABLE v.weather4(v VARCHAR(50));"
    $ pglift instance exec i1 -- psql -Udba2 d -c "SELECT * from v.weather4;"              
     v 
    ---
    (0 rows)
    ```

-   In some situation managing permission can become a nightmare, in the
    next section we will see what kind of problem we can face when we
    handle schema and multiple roles with extended permission (SELECT,
    INSERT, CREATE,...).

    TODO: Explain tricky situation with FOR ROLE when we allow a role
    (dba2) to create object on a schema, then when we give antoher role
    (`dba3`) some privileges on the same schema.

    ``` console
    ```

## Trickier Example multiple roles with read-write permission:

``` console
$ pglift instance exec i1 -- psql d -f set_perm_all.sql
```

We can then insert into table v.weather with any user:

``` console
$ pglift instance exec i1 -- psql -Udba1 d -c "INSERT INTO v.weather VALUES('1');" 
INSERT 0 1
$ pglift instance exec i1 -- psql -Udba2 d -c "INSERT INTO v.weather VALUES('2');"
INSERT 0 1
$ pglift instance exec i1 -- psql -Udba3 d -c "INSERT INTO v.weather VALUES('3');"
INSERT 0 1
```

But it won't work for a table created by dba2 or dba3 and used by the
other "non owner" role (Eg a table created by dba2 cannot be used by
dba3):

``` console
$ pglift instance exec i1 -- psql -Udba2 d -c "CREATE TABLE v.weather2(v VARCHAR(50));" 
```

``` console
$ pglift instance exec i1 -- psql -Udba3 d -c "INSERT INTO v.weather2 VALUES('x');"
ERROR:  permission denied for table weather2
```

This one fails because we don't alter default privileges with
`FOR ROLE dba2... TO dba3`:

To fix that, our current implementation loop over privileges table to
find other users with all, read-only or read-write profile and also
alter default privileges for them.

But that dba1 can use that table (it's the database and schema owner):

``` console
pglift instance exec i1 -- psql -Udba1 d -c "INSERT INTO v.weather2 VALUES('x');"
INSERT 0 1
```

## Example 2: With "group" of roles or meta role

After replaying init command, we alter default permission FOR ROLE dba1
to a meta role (group) called db_rw:

``` console
$ pglift instance exec i1 -- psql d -f set_perm_all_with_group_of_group.sql
```

When using the v schema, we face the same problem for object created by
a role other than dba1, that object / table cannot be used by another
role (Eg: role dba3 can't insert on table created by dba2)

<!--
        vim: spelllang=en spell
      -->
